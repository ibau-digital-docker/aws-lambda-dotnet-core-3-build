FROM mcr.microsoft.com/dotnet/core/sdk:3.1

RUN apt-get update && apt-get install -y zip

RUN dotnet tool install -g Amazon.Lambda.Tools \
    && ln -s /root/.dotnet/tools/dotnet-lambda /usr/bin/dotnet-lambda
        
RUN dotnet lambda --help
